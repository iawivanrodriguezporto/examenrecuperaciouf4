const mongoose = require('mongoose');
Schema = mongoose.Schema;

var rentSchema = new Schema({
	_id: mongoose.Types.ObjectId,
	client: String,
	car: String,
	price: Number
});

module.exports = mongoose.model('rent', rentSchema);