const mongoose = require('mongoose');
Schema = mongoose.Schema;

var carSchema = new Schema({
	id: String,
	brand: String,
	model: String,
	price: Number
});

module.exports = mongoose.model('cars', carSchema);