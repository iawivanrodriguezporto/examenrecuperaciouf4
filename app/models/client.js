const mongoose = require('mongoose');
Schema = mongoose.Schema;

var clientSchema = new Schema({
	id: String,
	name: String,
	surname: String
});

module.exports = mongoose.model('client', clientSchema);