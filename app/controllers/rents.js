var mongoose = require("mongoose"),
  Model = require("../models/rent");

exports.getAll = async (req, res, next) => {

};

exports.find = async (req, res, next) => {

};

exports.delete = async (req, res, next) => {
  
};

exports.add = async (req, res, next) => { 

  //price: Precio del coche x el numero de dias. PAra saber el precio del coche debes obtener ANTES la info del coche seleccionado
  var el = {
    car: req.body.car,
    client: req.body.client,
    days: req.body.days,
  };
  let price = req.params.chooseCar * parseInt(el.days);
  //Operaciones para guardar en la DB
  let mongooseId = mongoose.Types.ObjectId();
  let rent = await new Model(mongooseId ,el.clients, el.car, price);
  
  await rent.save();
  next();
};