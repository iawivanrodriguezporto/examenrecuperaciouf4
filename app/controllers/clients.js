var mongoose = require("mongoose"),
  Model = require("../models/client");

exports.getAll = async (req, res, next) => {
  let allClients = await Model.find({}).exec();
  req.params.clients = allClients;
  next();
};

exports.find = async (req, res, next) => {

};

exports.delete = async (req, res, next) => {

};
